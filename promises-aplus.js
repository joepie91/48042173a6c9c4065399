var pr = new Promise(function(resolve, reject) {
	resolve("a");
})

pr.then(function(val) {
	console.log(".then triggered");
})

console.log("after defining .then");